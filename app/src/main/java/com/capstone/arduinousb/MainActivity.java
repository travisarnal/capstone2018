package com.capstone.arduinousb;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorEventListener2;
import android.hardware.SensorManager;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.felhr.usbserial.UsbSerialDevice;
import com.felhr.usbserial.UsbSerialInterface;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.capstone.arduinousb.lib.*;
import static com.capstone.arduinousb.lib.KalmanLocationManager.UseProvider;
//import library.net.intari.KalmanLocationManager;

import static android.R.attr.gravity;
import static android.R.attr.hardwareAccelerated;
import static android.R.attr.wallpaperIntraCloseEnterAnimation;
import static android.hardware.SensorManager.getOrientation;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    /*
    section added for GPS functionality
    */
    boolean kalmanOn = true;
    LocationManager locationManager;
    double longitudeGPS, latitudeGPS;
    double localLongitude = -96.99299097;
    double localLatitude = 49.71037981;
    double mPerDegLat;
    double mPerDegLon;
    double longitudeMeters, latitudeMeters;
    TextView longitudeValueGPS, latitudeValueGPS;
    int MY_PERMISSIONS_REQUEST_FINE_LOCATION;
    double[] linear_acceleration = new double[3];
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private Sensor mRotation;
    private Sensor mMagnetometer;//, mGravity;
    private boolean mInitialized;
    private float mLastX, mLastY, mLastZ;
    private Button GPSButton;
    float Ax, Ay, Az;
    float Rx, Ry, Rz;
    float xAve, yAve, zAve;
    float rotationAngle;
    int readings;
    //float[] gravity;
    float[] mGeomagnetic;
    TextView accelX, accelY, accelZ;
    float azimuth, pitch, roll;
    long recentTime, prevTime,startTime;

    float[] prevAccel = new float[3];
    float[] nextAccel = new float[3];
    float[] prevVel = new float [3];
    float[] nextVel = new float[3];
    float[] prevDisp = new float[3];
    float[] nextDisp = new float[3];
    float[] gravity = new float[3];

    boolean GPSInit = false;
/*
    float[] accel0 = new float[3];
    float[] accel1 = new float[3];
    float[] accel2 = new float[3];
    float[] vel0 = new float[3];
    float[] vel1 = new float[3];
    float[] vel2 = new float[3];
    float[] disp0 = new float[3];
    float[] disp1 = new float[3];
    float[] disp2 = new float[3];*/

// assume only acceleration in the y direction.  Each array containts three consecutive values.
    float[] accel = new float[3];
    float[] vel = new float[3];
    float[] pos = new float[3];
    float[] time = new float[3];
    int iteration  = 1;

    float[] latitudeHist = new float[3];
    float[] longitudeHist = new float[3];


    // Constant

    /**
     * Request location updates with the highest possible frequency on gps.
     * Typically, this means one update per second for gps.
     */
    private static final long GPS_TIME = 1000;

    /**
     * For the network provider, which gives locations with less accuracy (less reliable),
     * request updates every 5 seconds.
     */
    private static final long NET_TIME = 5000;

    /**
     * For the filter-time argument we use a "real" value: the predictions are triggered by a timer.
     * Lets say we want 5 updates (estimates) per second = update each 200 millis.
     */
    private static final long FILTER_TIME = 200;


    int AccelSensor = Sensor.TYPE_ACCELEROMETER;

    private KalmanLocationManager mKalmanLocationManager;


    private boolean checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    private void requestKalman() {
        final AlertDialog.Builder prompt = new AlertDialog.Builder(this);
        prompt.setTitle("Select GPS type")
                .setMessage("Enable Kalman GPS Filtering?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        kalmanOn = true;
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        kalmanOn = false;
                    }
                });
        prompt.show();
    }

    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER); //||
               // locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    public void toggleKalman(View view) {
        if (kalmanOn){
            kalmanOn = false;
            mKalmanLocationManager.removeUpdates(locationListenerGPS);
            Toast.makeText(MainActivity.this, "Kalman Filter Disabled", Toast.LENGTH_SHORT).show();
        } else {
            kalmanOn = true;
            mKalmanLocationManager.requestLocationUpdates(
                            UseProvider.GPS_AND_NET, FILTER_TIME, GPS_TIME, NET_TIME, locationListenerGPS, true);
            Toast.makeText(MainActivity.this, "Kalman Filter Enabled", Toast.LENGTH_SHORT).show();

        }
    }
    public void resetGPS(View view) {
        localLatitude = latitudeGPS;
        localLongitude = longitudeGPS;
    }

    public void toggleGPSUpdates(View view) {
        if (!checkLocation())
            return;
        Button button = (Button) view;
        if (button.getText().equals(getResources().getString(R.string.pause))) {
            locationManager.removeUpdates(locationListenerGPS);


            button.setText(R.string.resume);
            Toast.makeText(MainActivity.this, "GPS Disabled", Toast.LENGTH_SHORT).show();
        } else {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)) {

                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_FINE_LOCATION);


                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
                //ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_FINE_LOCATION);
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                //return;
            }
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, GPS_TIME, 0, locationListenerGPS);

            //();
            //if (kalmanOn == true)  {
            //    mKalmanLocationManager.requestLocationUpdates(
            //            UseProvider.GPS_AND_NET, FILTER_TIME, GPS_TIME, NET_TIME, locationListenerGPS, true);
            //}
            button.setText(R.string.pause);
            Toast.makeText(MainActivity.this, "GPS Enabled", Toast.LENGTH_SHORT).show();


        }
    }

    private final LocationListener locationListenerGPS = new LocationListener() {
        public void onLocationChanged(Location location) {

            longitudeGPS = location.getLongitude();
            latitudeGPS = location.getLatitude();

            if (GPSInit == false){
                localLatitude = latitudeGPS;
                localLongitude = longitudeGPS;
                double lat = Math.toRadians(localLatitude);
                GPSInit = true;
                mPerDegLat = 111132.92 - 559.822 * Math.cos(2 * lat) +
                        1.175 * Math.cos(4 * lat) - 0.0023 * Math.cos(6 * lat);
                mPerDegLon = 111412.84 * Math.cos ( lat ) - 93.5*Math.cos(3*lat)
                        +0.118*Math.cos(5*lat);
            }
            //makeUseOfNewLocation(location);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    /*
                    longitudeMeters = (longitudeGPS-localLongitude)*111194.9266;
                    latitudeMeters = (longitudeGPS-localLongitude)*111194.9266*Math.cos((latitudeGPS)*2*Math.PI/360);
*/
                    //https://en.wikipedia.org/wiki/Geographic_coordinate_system#Expressing_latitude_and_longitude_as_linear_units
                    //https://gis.stackexchange.com/questions/75528/understanding-terms-in-length-of-degree-formula/75535#75535
                    //double latMid = (localLatitude+latitudeGPS)/2;
                    //latitudeMeters = latitudeGPS * mPerDegLat;
                    //longitudeMeters = longitudeGPS * mPerDegLon;

                    /*
                    double latMid = (localLatitude+latitudeGPS)/2;
                    latitudeMeters = 111132.92 - 559.822 * Math.cos( 2 * latMid ) +
                            1.175 * Math.cos( 4 * latMid) - 0.0023 * Math.cos(6*latMid);
                    longitudeMeters = 111412.84 * Math.cos ( latMid ) - 93.5*Math.cos(3*latMid)
                            +0.118*Math.cos(5*latMid);
*/

                    double deltaLat = (localLatitude - latitudeGPS);
                    double deltaLon = (localLongitude - longitudeGPS);

                    longitudeMeters = mPerDegLon * deltaLon;
                    latitudeMeters = mPerDegLat * deltaLat;
                    longitudeMeters = (double) Math.round(longitudeMeters*1000)/1000;
                    latitudeMeters = (double) Math.round(latitudeMeters*1000)/1000;

                    Log.i("Longitude/Latitude", Double.toString(longitudeMeters) +", " + Double.toString(latitudeMeters));
                    //Log.i("Latitude", Double.toString(latitudeMeters));

                    longitudeValueGPS.setText(longitudeMeters + "m");
                    latitudeValueGPS.setText(latitudeMeters + "m");
                    //tvAppend(textView,"GPS Update " + ((float) Math.round((recentTime-startTime)/10000000)/100) +
                     //       "s - (" + longitudeMeters + ", " + latitudeMeters + ")\n");
                    //Toast.makeText(MainActivity.this, "GPS Provider update", Toast.LENGTH_SHORT).show();
                }
            });
        }

        public double measure(double lat1, double lon1, double lat2, double lon2){  // generally used geo measurement function
            double R = 6378.137; // Radius of earth in KM
            double dLat = lat2 * Math.PI / 180 - lat1 * Math.PI / 180;
            double dLon = lon2 * Math.PI / 180 - lon1 * Math.PI / 180;
            double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                    Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
                            Math.sin(dLon/2) * Math.sin(dLon/2);
            double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
            double d = R * c;
            return d * 1000; // meters

        }
        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };
    /*
    End section for GPS functionality
     */


    public final String ACTION_USB_PERMISSION = "com.capstone.arduinousb.USB_PERMISSION";
    Button startButton, sendButton, clearButton, stopButton;
    TextView textView;
    EditText editText;
    UsbManager usbManager;
    UsbDevice device;
    UsbSerialDevice serialPort;
    UsbDeviceConnection connection;

    UsbSerialInterface.UsbReadCallback mCallback = new UsbSerialInterface.UsbReadCallback() { //Defining a Callback which triggers whenever data is read.
        @Override
        public void onReceivedData(byte[] arg0) {
            String data = null;
            try {
                data = new String(arg0, "UTF-8");
                data.concat("/n");
                if (data.equals("p")){
                    tvAppend(textView,"\n***Positional Data Packet Requested***");
                    sendPDP();
                } else if(data.equals("g")){
                    tvAppend(textView, "\n***GPS Coordinates Requested***");
                    sendGPScoordinates();
                } else if (data.equals("a")){
                    tvAppend(textView, "\n***Accelerometer Data Requested***");
                    sendAccelerometerData();
                } else {
                    tvAppend(textView, data);
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


        }
    };
    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() { //Broadcast Receiver to automatically start and stop the Serial connection.
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ACTION_USB_PERMISSION)) {
                boolean granted = intent.getExtras().getBoolean(UsbManager.EXTRA_PERMISSION_GRANTED);
                if (granted) {
                    connection = usbManager.openDevice(device);
                    serialPort = UsbSerialDevice.createUsbSerialDevice(device, connection);
                    if (serialPort != null) {
                        if (serialPort.open()) { //Set Serial Connection Parameters.
                            setUiEnabled(true);
                            serialPort.setBaudRate(9600);
                            serialPort.setDataBits(UsbSerialInterface.DATA_BITS_8);
                            serialPort.setStopBits(UsbSerialInterface.STOP_BITS_1);
                            serialPort.setParity(UsbSerialInterface.PARITY_NONE);
                            serialPort.setFlowControl(UsbSerialInterface.FLOW_CONTROL_OFF);
                            serialPort.read(mCallback);
                            tvAppend(textView,"Serial Connection Opened!\n");

                        } else {
                            Toast.makeText(MainActivity.this, "Serial Port Not Open", Toast.LENGTH_SHORT).show();
                            Log.d("SERIAL", "PORT NOT OPEN");
                        }
                    } else {
                        Log.d("SERIAL", "PORT IS NULL");
                        Toast.makeText(MainActivity.this, "Serial Port Null", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.d("SERIAL", "PERMISSION NOT GRANTED");
                    Toast.makeText(MainActivity.this, "Serial Permission not granted", Toast.LENGTH_SHORT).show();
                }

            } else if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_ATTACHED)) {
                onClickStart(startButton);
            } else if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_DETACHED)) {
                onClickStop(stopButton);

            }
        }

        ;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Start GPS

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        //longitudeValueBest = (TextView) findViewById(R.id.longitudeValueBest);
        //latitudeValueBest = (TextView) findViewById(R.id.latitudeValueBest);
        longitudeValueGPS = (TextView) findViewById(R.id.longitudeValueGPS);
        latitudeValueGPS = (TextView) findViewById(R.id.latitudeValueGPS);
        //longitudeValueNetwork = (TextView) findViewById(R.id.longitudeValueNetwork);
        //latitudeValueNetwork = (TextView) findViewById(R.id.latitudeValueNetwork);

        //end gps

        // Start Event Sensor
        mInitialized = false;
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(AccelSensor);
        mMagnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        //mGravity = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
        mRotation = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);


        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mMagnetometer, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mRotation, SensorManager.SENSOR_DELAY_NORMAL);
        //End Event Sensor


        usbManager = (UsbManager) getSystemService(this.USB_SERVICE);
        startButton = (Button) findViewById(R.id.buttonStart);
        sendButton = (Button) findViewById(R.id.buttonSend);
        clearButton = (Button) findViewById(R.id.buttonClear);
        stopButton = (Button) findViewById(R.id.buttonStop);
        editText = (EditText) findViewById(R.id.editText);
        textView = (TextView) findViewById(R.id.textView);
        setUiEnabled(false);
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_USB_PERMISSION);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        registerReceiver(broadcastReceiver, filter);

        GPSButton = (Button)findViewById(R.id.locationControllerGPS);
        GPSButton.performClick();
        textView.setMovementMethod(new ScrollingMovementMethod());

        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "MyWakelockTag");
        wakeLock.acquire();

        mKalmanLocationManager = new KalmanLocationManager(this);
    }

    public void setUiEnabled(boolean bool) {
        startButton.setEnabled(!bool);
        sendButton.setEnabled(bool);
        stopButton.setEnabled(bool);
        editText.setEnabled(bool);
        //textView.setEnabled(bool);

    }

    public void onClickStart(View view) {

        HashMap<String, UsbDevice> usbDevices = usbManager.getDeviceList();
        if (!usbDevices.isEmpty()) {
            boolean keep = true;
            for (Map.Entry<String, UsbDevice> entry : usbDevices.entrySet()) {
                device = entry.getValue();
                int deviceVID = device.getVendorId();
                if (deviceVID == 0x2341)//Arduino Vendor ID
                {
                    PendingIntent pi = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
                    usbManager.requestPermission(device, pi);
                    keep = false;
                    Toast.makeText(MainActivity.this, "Serial Connection Opened", Toast.LENGTH_SHORT).show();
                } else {
                    connection = null;
                    device = null;
                    Toast.makeText(MainActivity.this, "Serial Device Not Found", Toast.LENGTH_SHORT).show();
                }

                if (!keep)
                    break;
            }
        }




    }

    public void onClickSend(View view) {
        String string = editText.getText().toString();
        serialPort.write(string.getBytes());
        tvAppend(textView, "\nData Sent : " + string + "\n");

    }

    public void onClickStop(View view) {
        setUiEnabled(false);
        serialPort.close();
        tvAppend(textView,"\nSerial Connection Closed! \n");
        Toast.makeText(MainActivity.this, "Serial Connection Closed", Toast.LENGTH_SHORT).show();

    }

    public void onClickClear(View view) {

        textView.setText(" ");
        Arrays.fill(accel,0);
        Arrays.fill(vel,0);
        Arrays.fill(vel,0);
        iteration = 1;
    }

    private void tvAppend(TextView tv, CharSequence text) {
        final TextView ftv = tv;
        final CharSequence ftext = text;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ftv.append(ftext);
            }
        });
    }

    public void sendGPScoordinates() {
        String string = "Longitude: "+ longitudeValueGPS.getText().toString()
                +" Latitude: "+ latitudeValueGPS.getText().toString();
        serialPort.write(string.getBytes());
        tvAppend(textView, "\nData Sent : " + string + "\n");
    }

    public void sendAccelerometerData(){
        String string = "X:"+Float.toString(xAve)
                + " Y: " + Float.toString(yAve)
                + " Z: " + Float.toString(zAve);
        serialPort.write(string.getBytes());
        tvAppend(textView, "\nData Sent : " + string + "\n");
        resetAccel();
    }

    public void sendPDP(){


        String GPS = "GPS: ("+longitudeValueGPS.getText().toString()
                +", "+ latitudeValueGPS.getText().toString()+") ";
        String Accel = "Accel: (" + Ax + ", "+ Ay+", "+Az+") ";
        String Orientation = "Rotate: (" + azimuth+", "+pitch+", "+roll+") ";
        String time = "Time: ("+Long.toString(prevTime) + ", "+Long.toString(recentTime)+") ";
        String message = GPS + Accel + Orientation + time + "\n";

        //String message = GPS + "\\";
        //serialPort.write(message.getBytes());


        String endMessage = "\\";
        serialPort.write(double2ByteArray(longitudeMeters));
        serialPort.write(double2ByteArray(latitudeMeters));
        serialPort.write(float2ByteArray(Ax));
        serialPort.write(float2ByteArray(Ay));
        serialPort.write(float2ByteArray(Az));
        serialPort.write(float2ByteArray(azimuth));
        serialPort.write(float2ByteArray(pitch));
        serialPort.write(float2ByteArray(roll));
        serialPort.write(long2ByteArray(prevTime));
        serialPort.write(long2ByteArray(recentTime));
        serialPort.write(endMessage.getBytes());
        //double is 8 bytes, long is 8 bytes, float is 4 bytes
        //56 bytes total


        tvAppend(textView, "\n***PDP SENT***\n");
        //tvAppend(textView, message);
        resetAccel();
        prevTime = recentTime;
    }


    public static byte [] float2ByteArray (float value)
    {
        return ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putFloat(value).array();
    }

    public static byte [] double2ByteArray (double value)
    {
        return ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putDouble(value).array();
    }

    public static byte [] long2ByteArray (long value)
    {
        return ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putLong(value).array();
    }

    public void resetAccel(){
        readings= 0; xAve= 0; yAve = 0; zAve = 0;
    }




    //Start Motion sensor Stuff

    @Override
    protected void onResume() {
        super.onResume();
        // mSensorManager.registerListener(this,mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        //mSensorManager.registerListener(this,mMagnetometer, SensorManager.SENSOR_DELAY_NORMAL);
        if (kalmanOn == true) {
            mKalmanLocationManager.requestLocationUpdates(
                    UseProvider.GPS_AND_NET, FILTER_TIME, GPS_TIME, NET_TIME, locationListenerGPS, true);

        }
    }

    @Override
    protected void onPause(){
        super.onPause();
        //mSensorManager.unregisterListener(this);

    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        TextView tvX= (TextView)findViewById(R.id.x_axis);
        TextView tvY= (TextView)findViewById(R.id.y_axis);
        TextView tvZ= (TextView)findViewById(R.id.z_axis);
        TextView tvAccel= (TextView)findViewById(R.id.y_accel);
        TextView tvVel= (TextView)findViewById(R.id.y_vel);
        TextView tvPos= (TextView)findViewById(R.id.y_pos);
        TextView tvAzimuth= (TextView)findViewById(R.id.r_az);
        TextView tvPitch= (TextView)findViewById(R.id.r_pi);
        TextView tvRoll= (TextView)findViewById(R.id.r_ro);
        prevTime = recentTime;
        recentTime = event.timestamp;
        prevAccel = nextAccel;
        //TextView RtvAngle= (TextView)findViewById(R.id.r_angle);
        if (!mInitialized && (event.sensor.getType() == AccelSensor )) {
            Ax = event.values[0];
            Ay = event.values[1];
            Az = event.values[2];
            mLastX = Ax;
            mLastY = Ay;
            mLastZ = Az;
            tvX.setText("0.0");
            tvY.setText("0.0");
            tvZ.setText("0.0");
            mInitialized = true;
            resetAccel();

            prevAccel[0] = Ax; prevAccel[1] = Ay; prevAccel[2] = Az;
            //Arrays.fill(prevAccel,0);
            Arrays.fill(nextAccel,0);
            Arrays.fill(prevVel,0);
            Arrays.fill(prevDisp,0);

            prevTime = recentTime;
            startTime = recentTime;
            Arrays.fill(accel,0); Arrays.fill(vel,0); Arrays.fill(pos,0);
            time[2] = recentTime/1000000000;
            accel[2] = Ay;
        }
        if (mInitialized && event.sensor.getType() == AccelSensor){

            //ImageView iv = (ImageView)findViewById(R.id.image);
            gravity = event.values;
            if (!mInitialized) {/*
                Ax = event.values[0];
                Ay = event.values[1];
                Az = event.values[2];
                mLastX = Ax;
                mLastY = Ay;
                mLastZ = Az;
                tvX.setText("0.0");
                tvY.setText("0.0");
                tvZ.setText("0.0");
                mInitialized = true;
                resetAccel();

                Arrays.fill(prevAccel,0);
                Arrays.fill(nextAccel,0);*/

            } else {
                mLastX = Ax; mLastY = Ay; mLastZ = Az;
                Ax = event.values[0];
                Ay = event.values[1];
                Az = event.values[2];
                //Apply High pass filter to remove gravity
                /*
                float deltaX = Math.abs(mLastX - x);
                float deltaY = Math.abs(mLastY - y);
                float deltaZ = Math.abs(mLastZ - z);
                //null small values to filter noise
                if (deltaX < NOISE) deltaX = (float)0.0;
                if (deltaY < NOISE) deltaY = (float)0.0;
                if (deltaZ < NOISE) deltaZ = (float)0.0;*/

                //Try something new



                //Pass through LPF





/*
                float alpha = (float) 0.2;
                final float NOISE = (float) 0.01;
                mLastX = alpha * mLastX + (1 - alpha) * Ax;
                mLastY = alpha * mLastY + (1 - alpha) * Ay;
                mLastZ = alpha * mLastZ + (1 - alpha) * Az;
                Ax = Ax - mLastX;
                Ay = Ay - mLastY;
                Az = Az - mLastZ;
                if (Math.abs(Ax) < NOISE) Ax = (float) 0.0;

                if (Math.abs(Ay) < NOISE) Ay = (float) 0.0;

                if (Math.abs(Az) < NOISE) Az = (float) 0.0;
*/
                //Ax = Ax - xAve; Ay = Ay - yAve; Az = Az - zAve;

/*2018-02-09
          final float alpha = 0.8f;

          gravity[0] = alpha * gravity[0] + (1 - alpha) * event.values[0];
          gravity[1] = alpha * gravity[1] + (1 - alpha) * event.values[1];
          gravity[2] = alpha * gravity[2] + (1 - alpha) * event.values[2];

          Ax = event.values[0] - gravity[0];
          Ay = event.values[1] - gravity[1];
          Az = event.values[2] - gravity[2];

                Ay = LowPassFilter(Ay);
*/



                //Begin integration on accelerometer values to determine position
                if (iteration == 1){
                    accel[1] = accel[2]; accel[2] = Ay;
                    time[1] = time[2]; time[2] = recentTime/1000000000;

                    float[] tempAccel = {accel[1], accel[2]};
                    float[] tempTime = {time[2], time[1]};
                    vel[2] = 0 + integrationTrapz(tempAccel, tempTime);
                    float[] tempVel = {0,vel[2]};
                    pos[2] = 0 + integrationTrapz(tempVel, time);
                    iteration++;
                } else {
                    accel[0] = accel[1]; accel[1] = accel[2]; accel[2] = Ay;
                    vel[0] = vel[1]; vel[1] = vel[2];
                    pos[0] = pos[1]; pos[1] = pos[2];
                    time[0] = time[1]; time[1] = time[2]; time[2] = recentTime/1000000000;

                    vel[2] = vel[1] + integrationSimpson(accel, time);
                    pos[2] = pos[1] + integrationSimpson(vel, time);

                }
                tvAccel.setText(Float.toString(accel[2]));
                tvVel.setText(Float.toString(vel[2]));
                tvPos.setText(Float.toString(pos[2]));






                prevAccel[0] = Ax; prevAccel[1] = Ay; prevAccel[2] = Az;

                //end something new
                //
                //do function call here(Ax,Ay,Az)

                /*  Remove trap methdd, try simpsons method
                float[] xTemp = integrationTrapz(prevAccel[0], Ax, prevTime/1000000000,recentTime/1000000000,prevVel[0],prevDisp[0]);
                float[] yTemp = integrationTrapz(prevAccel[1], Ax, prevTime/1000000000,recentTime/1000000000,prevVel[1],prevDisp[1]);
                float[] zTemp = integrationTrapz(prevAccel[2], Ax, prevTime/1000000000,recentTime/1000000000,prevVel[2],prevDisp[2]);
                nextVel[0] = xTemp[0];
                nextDisp[0] = xTemp[1];
                nextVel[1] = yTemp[0];
                nextDisp[1] = yTemp[1];
                nextVel[2] = zTemp[0];
                nextDisp[2] = zTemp[1];

                prevDisp = nextDisp;
                prevVel = nextVel;
*/

                tvX.setText(Float.toString(Ax));
                tvY.setText(Float.toString(Ay));
                tvZ.setText(Float.toString(Az));
                //tvX.setText(Float.toString(nextDisp[0]));
                //tvY.setText(Float.toString(nextDisp[1]));
                //tvZ.setText(Float.toString(nextDisp[2]));

                //Begin Average accelerometer data for sending between accelerometer requests

                readings++;
                xAve = (xAve * (readings - 1) + Ax) / readings;
                yAve = (yAve * (readings - 1) + Ay) / readings;
                zAve = (zAve * (readings - 1) + Az) / readings;
            }
        }
        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
            mGeomagnetic = event.values;
        if (event.sensor.getType() == Sensor.TYPE_ORIENTATION) {
            azimuth = event.values[0] * (float) Math.PI / 180f;
            if (azimuth > Math.PI) azimuth -= 2 * Math.PI;
        }
        //if (event.sensor.getType() == Sensor.TYPE_GRAVITY)
            //gravity = event.values;
        if (gravity != null && mGeomagnetic != null) {
            float R[] = new float[9];
            float I[] = new float[9];
            boolean success = SensorManager.getRotationMatrix(R, I, gravity, mGeomagnetic);
            if (success) {
                float orientation[] = new float[3];
                SensorManager.getOrientation(R, orientation);
                //azimuth = orientation[0]; // orientation contains: azimuth, pitch and roll
                pitch = orientation[1];
                roll = orientation[2];
                tvAzimuth.setText(Float.toString(azimuth));
                tvPitch.setText(Float.toString(pitch));
                tvRoll.setText(Float.toString(roll));
            }
        }
        /*if (event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR){
            Rx = event.values[0];
            Ry = event.values[1];
            Rz = event.values[2];
            rotationAngle = event.values[3];
            //float[] rotation = new float[16];

            //float[] rotationMatrix = new float[4];
            //SensorManager.getRotationMatrix(rotationMatrix);
            //SensorManager.getRotationMatrixFromVector(rotation, event.values);

            //float[] orientationValues = new float[3];
            //SensorManager.getOrientation(rotation, orientationValues);
            //Rx = orientationValues[0];
            //Ry = orientationValues[1];
            //Rz = orientationValues[2];
            //rotationAngle = (float) 2*Math.acos(rotationAngle);
            //Rx = Rx / ((float) Math.sin(((rotationAngle)/2);
            //Ry = Ry / ((float) Math.sin((rotationAngle)/2);
            //Rz = Rz / ((float) Math.sin((rotationAngle)/2);

            //RtvX.setText(Float.toString(Rx));
            //RtvY.setText(Float.toString(Ry));
            //RtvZ.setText(Float.toString(Rz));
            //RtvAngle.setText((Float.toString(rotationAngle)));
        }
        */
    }
    /*
    @Override
    public void onSensorChanged(SensorEvent event)
    {
        // alpha is calculated as t / (t + dT)
        // with t, the low-pass filter's time-constant
        // and dT, the event delivery rate

        final float alpha = (float) 0.8;

        //gravity[0] = alpha * gravity[0] + (1 - alpha) * event.values[0];
        //gravity[1] = alpha * gravity[1] + (1 - alpha) * event.values[1];
        //gravity[2] = alpha * gravity[2] + (1 - alpha) * event.values[2];


        linear_acceleration[0] = event.values[0];// - gravity[0];
        linear_acceleration[1] = event.values[1];// - gravity[1];
        linear_acceleration[2] = event.values[2];// - gravity[2];
        accelX.setText(Double.toString(linear_acceleration[0]));
    }*/

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        //Do something
    }
/*
    public float[] integrationTrapz(float a1, float a2, float t1, float t2, float v1, float d1){
        float[] results =  new float[2];
        float v2 = v1 + (a1+a2)/2*(t2-t1);
        float d2 = d1 + (v1+v2)/2*(t2-t1);
        results[0] = v2;
        results[1] = d2;

        return results;
    }*/
    public float integrationTrapz(float[] x, float[] time){
        //takes two input samples and returns trapezoidal integration
        float result;
        result = (x[0]+x[1])/2*(time[1]-time[0]);

        return result;
    }


    public float integrationSimpson(float[] x, float[] time){
        //takes three input samples and returns Simpsons 1/3 integration
        float result;
        result = (x[0] + 4*x[1] + x[2])*(time[2]-time[0])/6;
        return result;
    }
    float smoothed   = 0;        // or some likely initial value
    float smoothing  = 1f;       // or whatever is desired
    //Date lastUpdate = new Date();

    public float LowPassFilter(float newValue ){

        double elapsedTime = Math.abs(recentTime-prevTime)/1000000000;
        smoothed += elapsedTime * ( newValue - smoothed ) / smoothing;
        return smoothed;
    }


}
